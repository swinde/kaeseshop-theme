[{block name="sw_footer_information"}]
    <ul class="information list-unstyled">

        [{oxifcontent ident="sw_aktuelles" object="_cont"}]
        <li><a href="[{ oxgetseourl ident="sw_aktuelles" type="oxcontent" }]">[{$_cont->oxcontents__oxtitle->value}]</a></li>
        [{/oxifcontent}]
        [{oxifcontent ident="sw_ueberuns" object="_cont"}]
        <li><a href="[{ oxgetseourl ident="sw_ueberuns" type="oxcontent" }]">[{$_cont->oxcontents__oxtitle->value}]</a></li>
        [{/oxifcontent}]
        [{oxifcontent ident="sw_wochenmaerkte" object="_cont"}]
        <li><a href="[{ oxgetseourl ident="sw_wochenmaerkte" type="oxcontent" }]">[{$_cont->oxcontents__oxtitle->value}]</a></li>
        [{/oxifcontent}]
        {*[{oxifcontent ident="cms_kaeseportrais" object="_cont"}]
        <li><a href="[{ oxgetseourl ident="cms_kaeseportrais" type="oxcontent" }]">[{$_cont->oxcontents__oxtitle->value}]</a></li>
        [{/oxifcontent}]*}
    </ul>
    [{/block}]