# Käseshop Theme

Kundentheme auf Grid Layout Basis für OXID Onlineshop

##Installation 

composer config repo.swinde/kaeseshop-theme git https://bitbucket.org/swinde/kaeseshop-theme.git

composer require --no-scripts --update-no-dev --no-interaction --optimize-autoloader swinde/kaeseshop-theme
